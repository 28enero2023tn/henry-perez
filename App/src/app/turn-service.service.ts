import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable, observable } from 'rxjs'
import { Turn } from './Turn';

@Injectable({
  providedIn: 'root'
})
export class TurnServiceService {
  url = 'http://localhost:90/henry-perez/Apii/'
  constructor(private http: HttpClient) { }

  getAllTurns():Observable<any>{
    return this.http.get(this.url+'read.php')
  }

  createTurn(turn:Turn):Observable<any>{
    return this.http.post(this.url+'create.php',turn)
  }

  updateTurn(turn:Turn):Observable<any>{
    return this.http.put(this.url+'update.php',turn)
  }
}
